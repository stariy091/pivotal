﻿namespace WebHookData.DataModel.JiraWebHook
{
    public class JiraAssignee
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
    }
}