﻿using System;

namespace WebHookData.DataModel.JiraWebHook
{
    public class JiraSprint
    {
        /// <summary>
        /// Do not use it for setting or comparing Task Custom field
        /// </summary>
        public int Id { get; set; }
        public string IdStr => Id.ToString();
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}