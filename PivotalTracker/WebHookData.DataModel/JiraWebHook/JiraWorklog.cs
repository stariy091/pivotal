﻿namespace WebHookData.DataModel.JiraWebHook
{
    public class JiraWorklog
    {
        public JiraAssignee Author { get; set; }
        public string Comment { get; set; }
        public string Started { get; set; }
        public long TimeSpentSeconds { get; set; }
        public string Id { get; set; }
        public string IssueId { get; set; }
    }
}