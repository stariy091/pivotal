﻿using System.Collections.Generic;

namespace WebHookData.DataModel.JiraWebHook
{
    public class JiraIssueField
    {
        public JiraIssueType IssueType { get; set; }
        public JiraProject Project { get; set; }
        public JiraStatus Status { get; set; }
        public JiraAssignee Assignee { get; set; }
        public List<JiraVersion> FixVersions { get; set; }
        public JiraIssue Parent { get; set; }
        public JiraWorklog Worklog { get; set; }
        public string Summary { get; set; }
    }
}