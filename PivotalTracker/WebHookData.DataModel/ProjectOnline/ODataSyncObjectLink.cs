﻿using System.Collections.Generic;

namespace WebHookData.DataModel.ProjectOnline
{
    public class ODataSyncObjectLink
    {
        public int SystemId { get; set; }
        public bool IsHomeProject { get; set; }
        public string ProjectId { get; set; }
        public string ProjectKey { get; set; }
        public List<string> Epics { get; set; }
    }
}