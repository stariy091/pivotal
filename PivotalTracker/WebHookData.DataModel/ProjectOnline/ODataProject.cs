﻿using System;
using System.Collections.Generic;

namespace WebHookData.DataModel.ProjectOnline
{
    public class ODataProject
    {
        public Guid ProjectId { get; set; }
        public double? PivotalTrackerProjectId { get; set; }
        public List<ODataSyncObjectLink> ODataSyncObjectLinks { get; set; }
    }
}