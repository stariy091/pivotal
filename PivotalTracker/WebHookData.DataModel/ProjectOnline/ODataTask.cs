﻿using System;

namespace WebHookData.DataModel.ProjectOnline
{
    public class ODataTask
    {
        public Guid ProjectId { get; set; }
        public Guid TaskId { get; set; }
        public Guid ParentTaskId { get; set; }
        public string SystemId { get; set; }
        public string IssueId { get; set; }
        public string ParentIssueId { get; set; }
        public string ParentEpicId { get; set; }
        public string ParentVersionId { get; set; }
    }
}