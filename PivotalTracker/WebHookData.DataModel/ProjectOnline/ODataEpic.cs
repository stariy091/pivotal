﻿using System.Collections.Generic;

namespace WebHookData.DataModel.ProjectOnline
{
    public class ODataEpic
    {
        public int SystemId { get; set; }
        public List<object> Epics { get; set; }
    }
}
