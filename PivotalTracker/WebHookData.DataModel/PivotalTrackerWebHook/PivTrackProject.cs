﻿using System;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackProject
    {
        public int Id { get; set; }
        public string Kind { get; set; }
        public string Name { get; set; }
        public DateTime Updated_at { get; set; }
    }
}