﻿namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackPerson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

    }
}