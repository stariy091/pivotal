﻿using System.Collections.Generic;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackRequest
    {
        public string Kind { get; set; }
        public string Guid { get; set; }
        public string Project_version { get; set; }
        public string Message { get; set; }
        public string Highlight { get; set; }
        public List<PivTrackerChangeLogItem> Changes { get; set; }
        public List<PivTrackerPrimaryResource> Primary_resources { get; set; }
        public PivTrackProject Project { get; set; }
    }
}