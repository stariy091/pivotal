﻿using System.Collections.Generic;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackerChangeLogItem
    {
        public string Kind { get; set; }
        public string Change_type { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Story_type { get; set; }
        public Dictionary<string, object> Original_values { get; set; }
        public Dictionary<string, object> New_values { get; set; }
    }
}