﻿using System;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackAccount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Updated_at { get; set; }
        public DateTime Created_at { get; set; }
    }
}