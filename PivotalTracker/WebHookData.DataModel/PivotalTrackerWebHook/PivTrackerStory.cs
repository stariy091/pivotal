﻿using System.Collections.Generic;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackerStory
    {
        public string Kind { get; set; }
        public int Id { get; set; }
        public string Story_type { get; set; }
        public string Name { get; set; }
        public string Current_state { get; set; }
        public int Project_id { get; set; }
        public List<int> Owner_ids { get; set; }
        public List<PivTrackLabel> Labels { get; set; }
        public int? Estimate { get; set; }
    }
}