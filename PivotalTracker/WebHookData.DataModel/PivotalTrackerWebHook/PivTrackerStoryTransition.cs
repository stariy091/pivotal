﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackerStoryTransition
    {
        public string Kind { get; set; }
        public string State { get; set; }
        public int Story_id { get; set; }
        public int ProjectId { get; set; }
    }
}
