﻿using System;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackLabel
    {
        public string Kind { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Project_id { get; set; }
        public DateTime Updated_at { get; set; }
        public DateTime Created_at { get; set; }
    }
}