﻿namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackerPrimaryResource
    {
        public string Kind { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Story_type { get; set; }
        public string Url { get; set; }
    }
}