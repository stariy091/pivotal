﻿using System;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackAccountMembership
    {
        public int Id { get; set; }
        public PivTrackPerson Person { get; set; }
        public DateTime Updated_at { get; set; }
        public DateTime Created_at { get; set; }
    }
}