﻿using System;

namespace WebHookData.DataModel.PivotalTrackerWebHook
{
    public class PivTrackerTask
    {
        public string Kind { get; set; }
        public int Id { get; set; }
        public int Story_id { get; set; }
        public string Description { get; set; }
        public bool Complete { get; set; }
        public DateTime Update_at { get; set; }
    }
}