﻿using System;

namespace WebHookData.DataModel.TfsWebHook
{
    public class TfsResponse
    {
        public string Data { get; set; }
        public Exception Exception { get; set; }
    }
}