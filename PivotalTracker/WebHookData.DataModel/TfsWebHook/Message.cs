﻿namespace WebHookData.DataModel.TfsWebHook
{
    public class Message
    {
        public string Text { get; set; }
    }
}