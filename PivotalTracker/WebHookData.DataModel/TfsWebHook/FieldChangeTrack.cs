﻿namespace WebHookData.DataModel.TfsWebHook
{
    public class FieldChangeTrack
    {
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}