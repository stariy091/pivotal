﻿namespace WebHookData.DataModel.Common
{
    public class ProjectServerCustomFieldNames
    {
        public const string SystemIdFieldName = "SixtyI_SystemId";
        public const string ParentEpicKeyFieldName = "SixtyI_ParentEpicKey";
        public const string IssueKeyFieldName = "SixtyI_IssueKey";
        public const string ParentIssueKeyFieldName = "SixtyI_ParentIssueKey";
    }
}