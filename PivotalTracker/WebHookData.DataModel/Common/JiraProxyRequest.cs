﻿using System;

namespace WebHookData.DataModel.Common
{
    public class JiraProxyRequest
    {
        public string PostData { get; set; }
        public string RequestType { get; set; }
        public int SystemId { get; set; }
        public string ApiUrl { get; set; }
        public Guid ProjectUid { get; set; }
    }
}