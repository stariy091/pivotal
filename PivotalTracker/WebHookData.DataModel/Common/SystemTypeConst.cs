﻿namespace WebHookData.DataModel.Common
{
    public class SystemTypeConst
    {
        public const int Jira = 1;
        public const int PivotalTracker = 2;
        public const int Tfs = 3;
    }
}
