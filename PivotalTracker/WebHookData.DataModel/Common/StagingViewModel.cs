﻿using System;
using WebHookData.DataModel.ProjectOnline;

namespace WebHookData.DataModel.Common
{
    public class StagingViewModel
    {
        public int StagingId { get; set; }
        public int SystemId { get; set; }
        public Guid? ProjectUid { get; set; }
        public string WebHookEvent { get; set; }
        public string ChangedFields { get; set; }
        public string ProjectId { get; set; }
        public string ProjectKey { get; set; }
        public string ProjectName { get; set; }
        public string IssueId { get; set; }
        public string IssueKey { get; set; }
        public string IssueTypeId { get; set; }
        public string IssueTypeName { get; set; }
        public string IssueName { get; set; }
        public string ParentEpicId { get; set; }
        public string ParentSprintId { get; set; }
        public string ParentSprintName { get; set; }
        public string ParentVersionId { get; set; }
        public string ParentVersionName { get; set; }
        public bool ParentVersionReleased { get; set; }
        public string ParentIssueId { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateFinish { get; set; }
        public string Assignee { get; set; }
        public string IssueStatus { get; set; }
        public Nullable<double> Estimate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string RecordState { get; set; }
        public string ParentEpicKey { get; set; }
        public string ParentIssueKey { get; set; }

        public ODataTask ODataTask { get; set; }
        public ODataTask ODataTaskParent { get; set; }
    }
}