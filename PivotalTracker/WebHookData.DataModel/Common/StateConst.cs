﻿namespace WebHookData.DataModel.Common
{
    public class StateConst
    {
        public const string New = "New";
        public const string InProgress = "InProgress";
        public const string Done = "Done";
        public const string Deleted = "Deleted";
    }
}