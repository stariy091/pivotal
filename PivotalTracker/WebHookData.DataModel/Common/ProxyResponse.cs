﻿using System;

namespace WebHookData.DataModel.Common
{
    public class ProxyResponse
    {
        public string Data { get; set; }
        public Exception Exception { get; set; }
    }
}