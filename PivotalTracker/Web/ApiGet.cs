﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using WebHookData.BL;
using WebHookData.DataModel.PivotalTrackerWebHook;

namespace WebHookWeb
{
    public static class ApiGet
    {
        static readonly string urlApi = "https://www.pivotaltracker.com/services/v5/";
        static readonly string Token = "";
        static T Get<T>(string url)
            where T : class, new()
        {
            T temp = new T();
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("X-TrackerToken", Token);
                var request = client.DownloadString(url);
                temp = JsonConvert.DeserializeObject<T>(request);
            }
            return temp;
        }
        public static IEnumerable<PivTrackProject> GetProjects(DateTime updateDate = default(DateTime))
        {
            var projects = Get<List<PivTrackProject>>(urlApi + "projects/").Where(d => d.Updated_at > updateDate).ToList();
            return projects;
        }
        public static IEnumerable<PivTrackerStory> GetStories(DateTime updateDate = default(DateTime))
        {
            List<PivTrackerStory> list = new List<PivTrackerStory>();
            string url = null;
            List<int> listIdProjects = GetProjects().Select(p => p.Id).ToList();
            var temp = (updateDate == default(DateTime)) ? "" : "?updated_after=" + updateDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
            foreach (var item in listIdProjects)
            {
                url = urlApi + "projects/" + item + "/stories/" + temp;
                list.AddRange(Get<List<PivTrackerStory>>(url));
            }

            return list;
        }
        public static IEnumerable<PivTrackerTask> GetTasks(DateTime updateDate = default(DateTime))
        {
            List<PivTrackerTask> list = new List<PivTrackerTask>();
            var storPrId = GetStories().Select(i => new { stId = i.Id, prId = i.Project_id });
            foreach (var item in storPrId)
            {
                var temp = Get<List<PivTrackerTask>>(urlApi + "projects/" + item.prId + "/stories/" + item.stId + "/tasks").Where(o => o.Update_at > updateDate);
                list.AddRange(temp);
            }
            return list;
        }
        public static IEnumerable<PivTrackerStoryTransition> GetStoryTransaction(DateTime updateDate = default(DateTime))
        {
            List<PivTrackerStoryTransition> list = new List<PivTrackerStoryTransition>();
            List<int> listIdProjects = GetProjects().Select(p => p.Id).ToList();
            string url = null;
            foreach (var item in listIdProjects)
            {
                url = urlApi + "projects/" + item + "/story_transitions";
                list.AddRange(Get<List<PivTrackerStoryTransition>>(url));
            }
            return list;
        }
        public static IEnumerable<PivTrackAccount> GetAccounts(DateTime updateDate = default(DateTime))
        {
            var accounts = Get<List<PivTrackAccount>>(urlApi + "accounts/").Where(d => d.Updated_at > updateDate).ToList();
            return accounts;
        }
        public static IEnumerable<PivTrackAccountMembership> GetAccountMemberships(DateTime updateDate = default(DateTime))
        {
            List<int> accountId = GetAccounts().Select(i => i.Id).ToList();
            List<PivTrackAccountMembership> accountMemberships = new List<PivTrackAccountMembership>();
            foreach (var item in accountId)
            {
                accountMemberships.AddRange(Get<List<PivTrackAccountMembership>>(urlApi + "accounts/" + item + "/memberships").Where(d => d.Updated_at > updateDate).ToList());
            }

            return accountMemberships;
        }
        public static IEnumerable<PivTrackLabel> GetLabels(DateTime updateDate = default(DateTime))
        {
            List<int> listIdProjects = GetProjects().Select(p => p.Id).ToList();
            List<PivTrackLabel> labels = new List<PivTrackLabel>();
            foreach (var item in listIdProjects)
            {
                labels.AddRange(Get<List<PivTrackLabel>>(urlApi + "projects/" + item + "/labels").Where(d => d.Updated_at > updateDate).ToList());
            }
            return labels;
        }

    }
}