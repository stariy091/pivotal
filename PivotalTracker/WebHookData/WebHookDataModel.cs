namespace WebHookData
{
    using System.Data.Entity;

    public partial class WebHookDataModel : DbContext
    {
        public WebHookDataModel()
            : base("name=WebHookDataModel")
        {
        }

        public virtual DbSet<WebHookEntry> WebHookEntries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
