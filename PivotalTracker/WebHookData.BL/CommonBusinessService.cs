﻿using System.Data.Entity;
using System.Threading.Tasks;
using WebHookData.DataAccess;
using WebHookData.DataAccess.ExternalAPI;

namespace WebHookData.BL
{
    public class CommonBusinessService
    {
        public async Task<JiraAccessService> GetJiraAccessServiceById(int systemId)
        {
            JiraAccessService jiraAccessService = null;
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                SyncSystem syncSystem =
                    await unitOfWork.SyncSystemRepository.GetQuery()
                    .FirstOrDefaultAsync(x => x.SystemId == systemId);
                if (syncSystem != null)
                {
                    jiraAccessService = new JiraAccessService(syncSystem);
                }
            }
            return jiraAccessService;
        }
    }
}