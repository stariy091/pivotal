﻿using System.Data.Entity;
using WebHookData.DataAccess.Repository.Base;

namespace WebHookData.DataAccess.Repository.Entity
{
    public class SyncSystemTypeRepository : GenericRepository<SyncSystemType>
    {
        public SyncSystemTypeRepository(DbContext context) : base(context)
        {
        }
    }
}