﻿using System;
using System.Data.Entity;
using System.Linq;
using WebHookData.DataAccess.Repository.Base;

namespace WebHookData.DataAccess.Repository.Entity
{
    public class SyncSystemRepository : GenericRepository<SyncSystem>
    {
        public SyncSystemRepository(DbContext context) : base(context)
        {
        }

        public SyncSystem GetSystemByUrl(string systemUrl)
        {
            SyncSystem syncSystem = GetQuery()
                .FirstOrDefault(x => x.SystemUrl.Equals(systemUrl, StringComparison.InvariantCultureIgnoreCase));
            return syncSystem;
        }
    }
}