﻿using System;
using System.Data.Entity;
using System.Linq;
using WebHookData.DataAccess.Repository.Base;

namespace WebHookData.DataAccess.Repository.Entity
{
    public class WorkLogEntryRepository : GenericRepository<WorkLogEntry>
    {
        public WorkLogEntryRepository(DbContext context) : base(context)
        {
        }

        public WorkLogEntry GetWorkLogById(int systemId, string id)
        {
            return DbSet.FirstOrDefault(x => x.SystemId == systemId && x.WorkLogId == id);
        }
    }
}