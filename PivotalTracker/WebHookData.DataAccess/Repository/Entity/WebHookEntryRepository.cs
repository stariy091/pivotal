﻿using System.Data.Entity;
using WebHookData.DataAccess.Repository.Base;

namespace WebHookData.DataAccess.Repository.Entity
{
    public class WebHookEntryRepository : GenericRepository<WebHookEntry>
    {
        public WebHookEntryRepository(DbContext context) : base(context)
        {
        }
    }
}