﻿using System.Data.Entity;
using System.Linq;
using WebHookData.DataAccess.Repository.Base;

namespace WebHookData.DataAccess.Repository.Entity
{
    public class StagingRepository : GenericRepository<Staging>
    {
        public StagingRepository(DbContext context) : base(context)
        {
        }

        public Staging GetIssueById(int systemId, string issueId, string issueTypeId)
        {
            return DbSet.FirstOrDefault(x => x.SystemId == systemId && x.IssueId == issueId && x.IssueTypeId == issueTypeId);
        }
    }
}