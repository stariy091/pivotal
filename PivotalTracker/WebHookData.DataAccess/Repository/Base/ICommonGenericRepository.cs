﻿using System.Collections.Generic;
using System.Linq;

namespace WebHookData.DataAccess.Repository.Base
{
    /// <summary>
    /// Repository patterns : http://www.asp.net/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface ICommonGenericRepository<TEntity> where TEntity : class
    {
        List<TEntity> GetList();
        IQueryable<TEntity> GetQuery();
        int GetCount();
        TEntity GetById(object id);
        void Insert(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entityToDelete);
        void DeleteRange(IEnumerable<TEntity> entitiesToDelete);
        int ExecuteSqlCommand(string command, params object[] parameters);
        string GetTableName();
    }
}