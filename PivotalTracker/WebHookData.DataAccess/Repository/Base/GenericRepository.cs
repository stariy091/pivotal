﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebHookData.DataAccess.Repository.Base
{
    /// <summary>
    /// Base class for repositories. More about Repository patterns : http://www.asp.net/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class GenericRepository<TEntity> : ICommonGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Db;
        protected readonly DbSet<TEntity> DbSet;

        public GenericRepository(DbContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public virtual List<TEntity> GetList()
        {
            return GetQuery().ToList();
        }

        
        public virtual IQueryable<TEntity> GetQuery()
        {
            return DbSet;
        }

        public int GetCount()
        {
            return GetQuery().Count();
        }

        public async Task<int> GetCountAsync()
        {
            return await GetQuery().CountAsync();
        }

        public virtual TEntity GetById(object id)
        {
            return DbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (Db.Entry(entityToDelete).State == EntityState.Detached)
            {
                DbSet.Attach(entityToDelete);
            }
            DbSet.Remove(entityToDelete);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entitiesToDelete)
        {
            foreach (var entityToDelete in entitiesToDelete)
            {
                if (Db.Entry(entityToDelete).State == EntityState.Detached)
                {
                    DbSet.Attach(entityToDelete);
                }
                DbSet.Remove(entityToDelete);
            }
        }

        public virtual void DeleteRangeAnonymous(IEnumerable<object> entitiesToDelete)
        {
            var castedList = entitiesToDelete.Cast<TEntity>().ToList();
            DeleteRange(castedList);
        }

        public int ExecuteSqlCommand(string command, params object[] parameters)
        {
            return Db.Database.ExecuteSqlCommand(command, parameters);
        }

        public string GetTableName()
        {
            ObjectContext objectContext = ((IObjectContextAdapter)Db).ObjectContext;
            return GetTableName(objectContext);
        }

        #region private methods

        private static string GetTableName(ObjectContext objectContext)
        {
            string sql = objectContext.CreateObjectSet<TEntity>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) AS");
            Match match = regex.Match(sql);

            string table = match.Groups["table"].Value;
            return table;
        }

        #endregion
    }
}