﻿using System;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using WebHookData.DataAccess.Repository.Entity;

namespace WebHookData.DataAccess
{
    public class UnitOfWork : IDisposable
    {
        public ProjectOnlineExtModel Context { get; private set; }

        public UnitOfWork()
        {
            Context = new ProjectOnlineExtModel();
        }

        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
                Context = null;
            }
        }

        public DbContextTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            AzureDbConfiguration.SuspendExecutionStrategy = true;
            return Context.Database.BeginTransaction(isolationLevel);
        }

        public void CommitTransaction(DbContextTransaction trans)
        {
            trans.Commit();
            AzureDbConfiguration.SuspendExecutionStrategy = false;
            trans.Dispose();
        }

        public void RollbackTransaction(DbContextTransaction trans)
        {
            if (trans != null)
            {
                trans.Rollback();
                AzureDbConfiguration.SuspendExecutionStrategy = false;
                trans.Dispose();
            }
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

        #region MasterRepository

        private MasterRepository masterRepository;
        public MasterRepository MasterRepository => masterRepository ?? (masterRepository = new MasterRepository(Context));

        #endregion

        #region StagingRepository

        private StagingRepository stagingRepository;
        public StagingRepository StagingRepository => stagingRepository ?? (stagingRepository = new StagingRepository(Context));

        #endregion

        #region WebHookEntryRepository

        private WebHookEntryRepository webHookEntryRepository;
        public WebHookEntryRepository WebHookEntryRepository => webHookEntryRepository ?? (webHookEntryRepository = new WebHookEntryRepository(Context));

        #endregion

        #region WorkLogEntryRepository

        private WorkLogEntryRepository workLogEntryRepository;
        public WorkLogEntryRepository WorkLogEntryRepository => workLogEntryRepository ?? (workLogEntryRepository = new WorkLogEntryRepository(Context));

        #endregion

        #region SyncSystemRepository

        private SyncSystemRepository syncSystemRepository;
        public SyncSystemRepository SyncSystemRepository => syncSystemRepository ?? (syncSystemRepository = new SyncSystemRepository(Context));

        #endregion

        #region SyncSystemTypeRepository

        private SyncSystemTypeRepository syncSystemTypeRepository;
        public SyncSystemTypeRepository SyncSystemTypeRepository => syncSystemTypeRepository ?? (syncSystemTypeRepository = new SyncSystemTypeRepository(Context));

        #endregion
    }
}