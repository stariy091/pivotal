﻿using System.Collections.Generic;
using Atlassian.Jira;

namespace WebHookData.DataAccess.ExternalAPI
{
    public static class JiraCacheHelper
    {
        public static Dictionary<int, List<CustomField>> SystemCustomFields { get; set; }

        public static List<Issue> Epics { get; set; }

        public static List<JiraUser> JiraUsers { get; set; }

        public static List<Project> Projects { get; set; }
    }
}