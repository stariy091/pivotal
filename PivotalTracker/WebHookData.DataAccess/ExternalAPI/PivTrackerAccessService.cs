﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using WebHookData.DataModel.PivotalTrackerWebHook;

namespace WebHookData.DataAccess.ExternalAPI
{
    public class PivTrackerAccessService
    {
        private readonly string pivTrackerUrl;
        private readonly string pivTrackerToken;

        public PivTrackerAccessService(string pivTrackerUrl, string pivTrackerToken)
        {
            this.pivTrackerUrl = pivTrackerUrl;
            this.pivTrackerToken = pivTrackerToken;
        }

        public List<PivTrackAccount> GetAccounts()
        {
            string response = ExecuteRequest("/accounts");
            return JsonConvert.DeserializeObject<List<PivTrackAccount>>(response);
        }

        public PivTrackAccountMembership GetPivTrackAccountMembership(int accountId, int personid)
        {
            string response = ExecuteRequest("/accounts/" + accountId + "/memberships/" + personid);
            return JsonConvert.DeserializeObject<PivTrackAccountMembership>(response);
        }

        public PivTrackerStory GetPivTrackerStory(int projectId, int storyId)
        {
            string response = ExecuteRequest("/projects/" + projectId + "/stories/" + storyId);
            return JsonConvert.DeserializeObject<PivTrackerStory>(response);
        }

        public List<PivTrackerTask> GetPivTrackerTasks(int projectId, int storyId)
        {
            string response = ExecuteRequest("/projects/" + projectId + "/stories/" + storyId + "/tasks");
            return JsonConvert.DeserializeObject<List<PivTrackerTask>>(response);
        }

        public string ExecuteRequest(string apiUrl)
        {
            var requestUrl = pivTrackerUrl + apiUrl;

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("X-TrackerToken", pivTrackerToken);

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream responseStream = httpWebResponse.GetResponseStream();
            if (responseStream != null)
            {
                StreamReader streamReader = new StreamReader(responseStream);
                string response = streamReader.ReadToEnd();
                return response;
            }
            return null;
        }

    }
}