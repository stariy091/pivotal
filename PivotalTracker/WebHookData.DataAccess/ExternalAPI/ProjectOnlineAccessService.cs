﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Xml.Linq;
using Microsoft.ProjectServer.Client;
using Microsoft.SharePoint.Client;
using WebHookData.DataModel.ProjectOnline;
using Newtonsoft.Json;

namespace WebHookData.DataAccess.ExternalAPI
{
    public class ProjectOnlineAccessService
    {
        public ProjectContext ProjectContext { get; }
        public DateTime CurrentDateForCompare { get; }

        private readonly string projectOnlineUrl, projectOnlineUserName, projectOnlinePassword;
        private readonly SecureString securePassword;
        private readonly bool isOnline;

        private const string psCustomFieldTfsEpicsName = "TfsEpicKey";

        private const string psCustomFieldSyncObjectLinkName = "SixtyI_SyncObjectLink";

        public ProjectOnlineAccessService(string projectOnlineUrl, string projectOnlineUserName, string projectOnlinePassword, bool isOnline)
        {
            this.projectOnlineUrl = projectOnlineUrl;
            this.projectOnlineUserName = projectOnlineUserName;
            this.projectOnlinePassword = projectOnlinePassword;
            this.isOnline = isOnline;
            securePassword = new SecureString();
            foreach (char c in projectOnlinePassword)
            {
                securePassword.AppendChar(c);
            }
            if (isOnline)
            {
                ProjectContext = new ProjectContext(projectOnlineUrl)
                {
                    Credentials = new SharePointOnlineCredentials(projectOnlineUserName, securePassword)
                };

            }
            else
            {
                ProjectContext = new ProjectContext(projectOnlineUrl)
                {
                    Credentials = new NetworkCredential(projectOnlineUserName, projectOnlinePassword)
                };
            }
            Web web = ProjectContext.Web;
            RegionalSettings regSettings = web.RegionalSettings;
            ProjectContext.Load(web);
            ProjectContext.Load(regSettings); //To get regional settings properties  
            Microsoft.SharePoint.Client.TimeZone projectOnlineTimeZone = regSettings.TimeZone;
            ProjectContext.Load(projectOnlineTimeZone);
            //To get the TimeZone propeties for the current web region settings  
            ProjectContext.ExecuteQuery();

            TimeSpan projectOnlineUtcOffset =
                TimeSpan.Parse(projectOnlineTimeZone.Description.Substring(4,
                    projectOnlineTimeZone.Description.IndexOf(")", StringComparison.Ordinal) - 4));
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            TimeZoneInfo projectOnlineTimeZoneInfo =
                timeZones.FirstOrDefault(x => x.BaseUtcOffset == projectOnlineUtcOffset);
            CurrentDateForCompare = projectOnlineTimeZoneInfo != null
                ? TimeZoneInfo.ConvertTime(DateTime.Now, projectOnlineTimeZoneInfo)
                : DateTime.Now;
        }

        public Dictionary<string, string> GetCustomFields()
        {
            if (ProjectOnlineCacheHelper.CustomFields == null)
            {
                ProjectContext.Load(ProjectContext.CustomFields);
                ProjectContext.ExecuteQuery();
                ProjectOnlineCacheHelper.CustomFields = ProjectContext.CustomFields.ToList();
            }
            return ProjectOnlineCacheHelper.CustomFields.ToDictionary(customField => customField.Name, customField => customField.InternalName);
        }

        public bool WaitForQueue(QueueJob job)
        {
            JobState state = ProjectContext.WaitForQueue(job, int.MaxValue);
            return state == JobState.Success;
        }

        //public ODataProject GetODataProject(string odataUrl)
        //{
        //    string response = ExecuteRequest(odataUrl);
        //    if (!String.IsNullOrEmpty(response))
        //    {
        //        XDocument document = XDocument.Parse(response);
        //        if (document.Root != null)
        //        {
        //            XElement entryElement = document.Root.Elements().FirstOrDefault(x => x.Name.LocalName == "entry");
        //            if (entryElement != null)
        //            {
        //                XElement contentElement = entryElement.Elements().FirstOrDefault(x => x.Name.LocalName == "content");
        //                if (contentElement != null)
        //                {
        //                    XElement propertiesElement = contentElement.Elements().FirstOrDefault(x => x.Name.LocalName == "properties");
        //                    if (propertiesElement != null)
        //                    {
        //                        var project = new ODataProject();
        //                        IEnumerable<XElement> propertiesElements = propertiesElement.Elements();
        //                        foreach (XElement xElement in propertiesElements)
        //                        {
        //                            switch (xElement.Name.LocalName)
        //                            {
        //                                case "ProjectId":
        //                                    project.ProjectId = Guid.Parse(xElement.Value);
        //                                    break;
        //                                case "JiraObjectKey":
        //                                    project.JiraObjectKey = xElement.Value;
        //                                    break;
        //                                case "PivotalTrackerProjectId":
        //                                    project.PivotalTrackerProjectId = Double.Parse(xElement.Value, CultureInfo.InvariantCulture);
        //                                    break;
        //                            }
        //                        }
        //                        return project;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return null;
        //}

        public List<ODataProject> GetODataProjects(string odataUrl)
        {
            List<ODataProject> projects = new List<ODataProject>();
            string response = ExecuteRequest(odataUrl);
            if (!String.IsNullOrEmpty(response))
            {
                XDocument document = XDocument.Parse(response);
                if (document.Root != null)
                {
                    IEnumerable<XElement> entryElements = document.Root.Elements().Where(x => x.Name.LocalName == "entry");
                    foreach (XElement entryElement in entryElements)
                    {
                        XElement contentElement = entryElement.Elements().FirstOrDefault(x => x.Name.LocalName == "content");
                        if (contentElement != null)
                        {
                            XElement propertiesElement = contentElement.Elements().FirstOrDefault(x => x.Name.LocalName == "properties");
                            if (propertiesElement != null)
                            {
                                var project = new ODataProject();
                                IEnumerable<XElement> propertiesElements = propertiesElement.Elements();
                                foreach (XElement xElement in propertiesElements)
                                {
                                    switch (xElement.Name.LocalName)
                                    {
                                        case "ProjectId":
                                            project.ProjectId = Guid.Parse(xElement.Value);
                                            break;
                                        //case psCustomFieldTfsEpicsName:
                                        //    project.ODataEpicLinks = JsonConvert.DeserializeObject<List<ODataEpicLink>>(xElement.Value);
                                        //    break;
                                        case psCustomFieldSyncObjectLinkName:
                                            project.ODataSyncObjectLinks = JsonConvert.DeserializeObject<List<ODataSyncObjectLink>>(xElement.Value);
                                            break;
                                        //case psCustomFieldJiraProjectName:
                                        //    project.ODataProjectLinks = JsonConvert.DeserializeObject<List<ODataProjectLink>>(xElement.Value);
                                        //    break;
                                        case "PivotalTrackerProjectId":
                                            project.PivotalTrackerProjectId = Double.Parse(xElement.Value, CultureInfo.InvariantCulture);
                                            break;
                                    }
                                }
                                projects.Add(project);
                            }
                        }
                    }
                }
            }
            return projects;
        }

        public List<ODataProject> GetODataProjectsForTfs()
        {
            string odataUrl = projectOnlineUrl + "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,"
                + psCustomFieldTfsEpicsName + "&$filter=" + psCustomFieldTfsEpicsName + " ne null";
            return GetODataProjects(odataUrl);
        }

        public List<ODataProject> GetODataProjectsForJiraWithoutProject(Guid projectId)
        {
            string odataUrl = projectOnlineUrl + "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,"
                + psCustomFieldSyncObjectLinkName
                + "&$filter=" + psCustomFieldSyncObjectLinkName + " ne null and ProjectId ne guid'" + projectId + "'";
            return GetODataProjects(odataUrl);
        }

        public List<ODataProject> GetODataProjectsForJira()
        {
            string odataUrl = projectOnlineUrl + "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,"
                + psCustomFieldSyncObjectLinkName
                + "&$filter=" + psCustomFieldSyncObjectLinkName + " ne null";
            return GetODataProjects(odataUrl);
        }

        public ODataProject GetODataProjectForJira(Guid projectId)
        {
            string odataUrl = projectOnlineUrl + "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,"
                + psCustomFieldSyncObjectLinkName
                + "&$filter=ProjectId eq guid'" + projectId + "'";
            List<ODataProject> oDataProjects = GetODataProjects(odataUrl);
            if (oDataProjects != null && oDataProjects.Count != 0)
            {
                return oDataProjects[0];
            }
            return null;
        }

        //public ODataProject GetODataProjectForPivotalTracker(string pivotalTrackerProjectId)
        //{
        //    //string odataUrl = projectOnlineUrl + "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,JiraObjectKey,PivotalTrackerProjectId&$filter=PivotalTrackerProjectId eq " + pivotalTrackerProjectId;
        //    string odataUrl = projectOnlineUrl +
        //          "/_api/ProjectData/Projects?$select=ProjectId,ProjectName,JiraObjectKey,PivotalTrackerProjectId&$filter=substringof('" +
        //          pivotalTrackerProjectId + "',PivotalTrackerProjectId)";
        //    return GetODataProject(odataUrl);
        //}

        public List<ODataTask> GetODataTasks(string issueIds)
        {
            string odataUrl = projectOnlineUrl + "/_api/ProjectData/Tasks?$select=ProjectId,TaskId,ParentTaskId,SystemId,IssueId,ParentIssueId,ParentEpicId,ParentVersionId&$filter=substringof(IssueId,'" + issueIds + "')";
            string response = ExecuteRequest(odataUrl);
            List<ODataTask> tasks = new List<ODataTask>();
            if (!String.IsNullOrEmpty(response))
            {
                XDocument document = XDocument.Parse(response);
                if (document.Root != null)
                {
                    IEnumerable<XElement> entryElements = document.Root.Elements().Where(x => x.Name.LocalName == "entry");
                    foreach (XElement entryElement in entryElements)
                    {
                        XElement contentElement = entryElement.Elements().FirstOrDefault(x => x.Name.LocalName == "content");
                        if (contentElement != null)
                        {
                            XElement propertiesElement = contentElement.Elements().FirstOrDefault(x => x.Name.LocalName == "properties");
                            if (propertiesElement != null)
                            {
                                var task = new ODataTask();
                                IEnumerable<XElement> propertiesElements = propertiesElement.Elements();
                                foreach (XElement xElement in propertiesElements)
                                {
                                    switch (xElement.Name.LocalName)
                                    {
                                        case "ProjectId":
                                            task.ProjectId = Guid.Parse(xElement.Value);
                                            break;
                                        case "TaskId":
                                            task.TaskId = Guid.Parse(xElement.Value);
                                            break;
                                        case "ParentTaskId":
                                            task.ParentTaskId = Guid.Parse(xElement.Value);
                                            break;
                                        case "SystemId":
                                            task.SystemId = xElement.Value;
                                            break;
                                        case "IssueId":
                                            task.IssueId = xElement.Value;
                                            break;
                                        case "ParentIssueId":
                                            task.ParentIssueId = xElement.Value;
                                            break;
                                        case "ParentEpicId":
                                            task.ParentEpicId = xElement.Value;
                                            break;
                                        case "ParentVersionId":
                                            task.ParentVersionId = xElement.Value;
                                            break;
                                    }
                                }
                                tasks.Add(task);
                            }
                        }
                    }
                }
            }
            return tasks;
        }

        private string ExecuteRequest(string odataUrl)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(odataUrl);

            if (isOnline)
            {
                httpWebRequest.Credentials = new SharePointOnlineCredentials(projectOnlineUserName, securePassword);
            }
            else
            {
                httpWebRequest.Credentials = new NetworkCredential(projectOnlineUserName, projectOnlinePassword);
            }
            //httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream responseStream = httpWebResponse.GetResponseStream();
            if (responseStream != null)
            {
                StreamReader streamReader = new StreamReader(responseStream);
                string response = streamReader.ReadToEnd();
                return response;
            }
            return null;
        }

        public PublishedProject GetPublishedProject(Guid projectUid)
        {
            PublishedProject project = ProjectContext.Projects.GetByGuid(projectUid);
            if (isOnline)
            {
                ProjectContext.Load(project, p => p, p => p.CheckedOutBy, p => p.IncludeCustomFields);
            }
            else
            {
                ProjectContext.Load(project, p => p, p => p.IncludeCustomFields);
            }
            ProjectContext.Load(project.Tasks,
                c =>
                    c.IncludeWithDefaultProperties(t => t.Assignments,
                        t => t.Assignments.IncludeWithDefaultProperties(a => a.Resource, a => a.Resource.EnterpriseResource)));
            ProjectContext.Load(project.ProjectResources,
                c => c.IncludeWithDefaultProperties(pr => pr.EnterpriseResource.Id, pr => pr.EnterpriseResource.Email));
            ProjectContext.ExecuteQuery();
            return project;
        }

        public DraftProject GetDraftProject(PublishedProject publishedProject)
        {
            DraftProject draftProject = publishedProject.CheckOut();
            return GetDraftProject(draftProject);
        }

        public DraftProject GetDraftProject(DraftProject draftProject)
        {
            ProjectContext.Load(draftProject);
            ProjectContext.Load(draftProject.ProjectResources);
            ProjectContext.Load(draftProject.Tasks,
                c =>
                    c.IncludeWithDefaultProperties(t => t.Assignments,
                        t => t.Assignments.IncludeWithDefaultProperties(a => a.Resource)));
            ProjectContext.ExecuteQuery();
            return draftProject;
        }

        public EnterpriseResource GetResource(Guid id)
        {
            EnterpriseResource resource = ProjectContext.EnterpriseResources.GetByGuid(id);
            ProjectContext.Load(resource);
            ProjectContext.ExecuteQuery();
            return resource;
        }

        public EnterpriseResource GetResource(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                return null;
            }
            EnterpriseResourceCollection resources = ProjectContext.EnterpriseResources;
            ProjectContext.Load(resources, c => c.Where(x => x.Email == email));
            ProjectContext.ExecuteQuery();
            return resources.FirstOrDefault();
        }
    }
}