﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using RestSharp;
using WebHookData.DataModel.Common;
using System.Data.Entity;
using WebHookData.DataModel.JiraWebHook;

namespace WebHookData.DataAccess.ExternalAPI
{
    public class JiraAccessService
    {
        public JiraAccessService(SyncSystem syncSystem)
        {
            jiraUserName = syncSystem.SystemLogin;
            jiraPassword = syncSystem.SystemPassword;
            jiraApiUrl = syncSystem.SystemApiUrl.Trim('/');
            JiraConnection = Jira.CreateRestClient(syncSystem.SystemUrl, jiraUserName, jiraPassword);
        }

        public Jira JiraConnection { get; }

        private readonly string jiraUserName, jiraPassword, jiraApiUrl;

        private string EncodeTo64(string userName, string password)
        {
            string mergedCredentials = $"{userName}:{password}";
            byte[] toEncodeAsBytes = Encoding.ASCII.GetBytes(mergedCredentials);
            string returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public async Task<string> GetJiraResponse(JiraProxyRequest jiraRequest)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(jiraApiUrl + "/" + jiraRequest.ApiUrl.Trim('/'));
            httpWebRequest.Method = jiraRequest.RequestType;
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization", "Basic " + EncodeTo64(jiraUserName, jiraPassword));
            if ((jiraRequest.RequestType == "POST" || jiraRequest.RequestType == "PUT") &&
                !String.IsNullOrEmpty(jiraRequest.PostData))
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(jiraRequest.PostData);
                httpWebRequest.ContentLength = byteArray.Length;
                Stream dataStream = httpWebRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            HttpWebResponse httpWebResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();
            Stream responseStream = httpWebResponse.GetResponseStream();
            if (responseStream != null)
            {
                StreamReader streamReader = new StreamReader(responseStream);
                string response = await streamReader.ReadToEndAsync();
                return response;
            }
            return null;
        }

        #region Projects

        public async Task<List<Project>> GetProjects()
        {
            return JiraCacheHelper.Projects ??
                   (JiraCacheHelper.Projects =
                       (await JiraConnection.Projects.GetProjectsAsync()).Take(int.MaxValue).ToList());
        }

        public async Task<Project> GetProjectById(string projectId)
        {
            Project project = (await GetProjects()).FirstOrDefault(x => x.Id == projectId);
            if (project == null)
            {
                JiraCacheHelper.Projects = null;
                return (await GetProjects()).FirstOrDefault(x => x.Id == projectId);
            }
            return project;
        }

        public async Task<Project> GetProjectByKey(string projectKey)
        {
            Project project = (await GetProjects()).FirstOrDefault(x => x.Key == projectKey);
            if (project == null)
            {
                JiraCacheHelper.Projects = null;
                return (await GetProjects()).FirstOrDefault(x => x.Key == projectKey);
            }
            return project;
        }

        #endregion

        #region Issues

        public async Task<List<Issue>> GetIssuesAsync(string jql)
        {
            int itemsPerPage = 1000;
            int counter = 0;
            List<Issue> result = new List<Issue>();

            IPagedQueryResult<Issue> query = await JiraConnection.Issues
                    .GetIsssuesFromJqlAsync(jql, itemsPerPage, counter);
            counter += itemsPerPage;

            while (query.TotalItems >= counter)
            {
                counter += itemsPerPage;
                query = await JiraConnection.Issues
                    .GetIsssuesFromJqlAsync(jql, itemsPerPage, counter);
                result.AddRange(query.ToList());
            }
            return query.ToList();
        }

        #endregion

        #region Users

        public async Task<JiraUser> GetJiraUser(string userEmail)
        {
            if (JiraCacheHelper.JiraUsers == null)
            {
                JiraCacheHelper.JiraUsers = new List<JiraUser>();
            }
            JiraUser jiraUser = JiraCacheHelper.JiraUsers.FirstOrDefault(x => x.Email.ToLower() == userEmail.ToLower());
            if (jiraUser != null)
            {
                return jiraUser;
            }
            jiraUser = (await JiraConnection.RestClient.ExecuteRequestAsync<JiraUser[]>(Method.GET, "rest/api/latest/user/search?username=" + userEmail)).FirstOrDefault();
            if (jiraUser != null)
            {
                JiraCacheHelper.JiraUsers.Add(jiraUser);
            }
            return jiraUser;
        }

        #endregion

        #region CustomFields

        public async Task<List<CustomField>> GetCustomFields(int systemId)
        {
            if (JiraCacheHelper.SystemCustomFields == null)
            {
                JiraCacheHelper.SystemCustomFields = new Dictionary<int, List<CustomField>>();
            }
            if (!JiraCacheHelper.SystemCustomFields.ContainsKey(systemId))
            {
                List<CustomField> customFields = (await JiraConnection.Fields.GetCustomFieldsAsync())
                    .Take(int.MaxValue).ToList();
                JiraCacheHelper.SystemCustomFields.Add(systemId, customFields);
            }
            return JiraCacheHelper.SystemCustomFields[systemId];
        }

        #endregion

        #region Versions

        public async Task<IEnumerable<ProjectVersion>> GetVersionsAsync(string projectKey)
        {
            return await JiraConnection.Versions.GetVersionsAsync(projectKey);
        }

        #endregion

        #region WorkLog

        public async Task<JiraWorklog> GetJiraWorkLog(string issueId, string workLogId)
        {
            JiraWorklog jiraWorklog = await JiraConnection.RestClient.ExecuteRequestAsync<JiraWorklog>(Method.GET, "rest/api/latest/issue/" + issueId + "/worklog/" + workLogId);
            return jiraWorklog;
        }

        #endregion

        public async Task RefreshCache()
        {
            JiraCacheHelper.JiraUsers = null;
            JiraCacheHelper.SystemCustomFields = null;
            JiraCacheHelper.Epics = null;
            JiraCacheHelper.Projects = null;
            await GetProjects();
            //Task.WaitAll(GetProjects(), GetCustomFields(), GetEpics());
        }
    }
}