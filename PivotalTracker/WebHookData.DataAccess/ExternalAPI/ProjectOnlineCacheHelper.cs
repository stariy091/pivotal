﻿using System.Collections.Generic;
using Microsoft.ProjectServer.Client;

namespace WebHookData.DataAccess.ExternalAPI
{
    public static class ProjectOnlineCacheHelper
    {
        public static List<CustomField> CustomFields { get; set; }
    }
}