﻿CREATE TABLE [dbo].[WebHookEntry] (
    [EntryId]        INT            IDENTITY (1, 1) NOT NULL,
    [MasterId]       INT            NULL,
    [JsonRequest]    NVARCHAR (MAX) NOT NULL,
    [DateCreated]    DATETIME       NOT NULL,
    [WorkLogEntryId] INT            NULL,
    CONSTRAINT [PK_dbo.WebHookEntry] PRIMARY KEY CLUSTERED ([EntryId] ASC),
    CONSTRAINT [FK_WebHookEntry_Master] FOREIGN KEY ([MasterId]) REFERENCES [dbo].[Master] ([MasterId]) ON DELETE CASCADE,
    CONSTRAINT [FK_WebHookEntry_WorkLogEntry] FOREIGN KEY ([WorkLogEntryId]) REFERENCES [dbo].[WorkLogEntry] ([WorkLogEntryId]) ON DELETE CASCADE
);





