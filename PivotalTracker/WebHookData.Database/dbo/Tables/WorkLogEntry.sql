﻿CREATE TABLE [dbo].[WorkLogEntry] (
    [WorkLogEntryId]   INT            IDENTITY (1, 1) NOT NULL,
    [WorkLogId]        NVARCHAR (MAX) NULL,
    [SystemId]         INT            NOT NULL,
    [IssueId]          NVARCHAR (50)  NULL,
    [TimeSpentSeconds] BIGINT         NULL,
    [DateStarted]      DATETIME       NULL,
    [DateCreated]      DATETIME       NOT NULL,
    [DateUpdated]      DATETIME       NOT NULL,
    [Comment]          NVARCHAR (MAX) NULL,
    [AuthorEmailAddress]        NVARCHAR (MAX) NULL,
    [RecordState]      NVARCHAR (50)  NOT NULL,
    [AuthorName] NVARCHAR(50) NULL, 
    [AuthorKey] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_WorkLogEntry] PRIMARY KEY CLUSTERED ([WorkLogEntryId] ASC),
    CONSTRAINT [FK_WorkLogEntry_SyncSystem] FOREIGN KEY ([SystemId]) REFERENCES [dbo].[SyncSystem] ([SystemId])
);


