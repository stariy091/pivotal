﻿CREATE TABLE [dbo].[Staging] (
    [StagingId]             INT              IDENTITY (1, 1) NOT NULL,
    [SystemId]              INT              NOT NULL,
    [ProjectUid]            UNIQUEIDENTIFIER NULL,
    [WebHookEvent]          NVARCHAR (MAX)   NOT NULL,
    [ChangedFields]         NVARCHAR (MAX)   NULL,
    [ProjectId]             NVARCHAR (50)    NULL,
    [ProjectKey]            NVARCHAR (MAX)   NULL,
    [ProjectName]           NVARCHAR (MAX)   NULL,
    [IssueId]               NVARCHAR (50)    NULL,
    [IssueKey]              NVARCHAR (MAX)   NULL,
    [IssueTypeId]           NVARCHAR (50)    NULL,
    [IssueTypeName]         NVARCHAR (50)    NULL,
    [IssueName]             NVARCHAR (MAX)   NULL,
    [ParentEpicId]          NVARCHAR (50)    NULL,
    [ParentEpicKey]          NVARCHAR (50)    NULL,
    [ParentSprintId]        NVARCHAR (50)    NULL,
    [ParentSprintName]      NVARCHAR (MAX)   NULL,
    [ParentVersionId]       NVARCHAR (50)    NULL,
    [ParentVersionName]     NVARCHAR (MAX)   NULL,
    [ParentVersionReleased] BIT              NOT NULL,
    [ParentIssueId]         NVARCHAR (50)    NULL,
    [ParentIssueKey]         NVARCHAR (50)    NULL,
    [DateStart]             DATETIME         NULL,
    [DateFinish]            DATETIME         NULL,
    [Assignee]              NVARCHAR (MAX)   NULL,
    [IssueStatus]           NVARCHAR (MAX)   NULL,
    [Estimate]              FLOAT (53)       NULL,
    [DateCreated]           DATETIME         NOT NULL,
    [DateUpdated]           DATETIME         NOT NULL,
    [RecordState]           NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_Stating] PRIMARY KEY CLUSTERED ([StagingId] ASC),
    CONSTRAINT [FK_Stating_SyncSystem] FOREIGN KEY ([SystemId]) REFERENCES [dbo].[SyncSystem] ([SystemId]) ON DELETE CASCADE
);



