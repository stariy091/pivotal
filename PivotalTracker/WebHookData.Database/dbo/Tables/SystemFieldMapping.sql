﻿CREATE TABLE [dbo].[SystemFieldMapping]
(
	[SystemFieldMappingId] INT NOT NULL PRIMARY KEY, 
    [SystemFieldName] NVARCHAR(MAX) NOT NULL, 
    [ProjectServerFieldName] NVARCHAR(MAX) NOT NULL
)
